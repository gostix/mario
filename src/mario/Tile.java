package mario;


import java.awt.Image;
import java.awt.Rectangle;

public class Tile {

	private Rectangle r;
	private int tileX, tileY, speedX, type;
	public Image tileImage;

	private Mario mario = StartingClass.getRobot();
	private Background bg = StartingClass.getBg1();

	public Tile(int x, int y, int typeInt) {
		tileX = x * 32;
		tileY = y * 32;


		
		type = typeInt;

		r = new Rectangle();

		if (type == 5) {
			tileImage = StartingClass.tiledirt;
		} else if (type == 8) {
			tileImage = StartingClass.tilegrassTop;
		} else if (type == 4) {
			tileImage = StartingClass.tilegrassLeft;

		} else if (type == 6) {
			tileImage = StartingClass.tilegrassRight;

		} else if (type == 2) {
			tileImage = StartingClass.tilegrassBot;
		} else {
			type = 0;
		}

	}


	public void checkVerticalCollision(Rectangle rtop) {
		
		 if (type != 2 && mario.getSpeedY() >= 1) {
	            mario.setJumped(false);
	            mario.setSpeedY(0);
	            mario.setCenterY(tileY); 
	        }
		 
		 if(type == 2){
			 mario.setSpeedY(Math.abs(mario.getSpeedY()));
		 }
		 
	}


	public void checkSideCollision(Rectangle rtop) {
		mario.setSpeedX(0);
		if(rtop.equals(Mario.rectLeft))
			mario.setCenterX(tileX + 40);
		if(rtop.equals(Mario.rectRight))
			mario.setCenterX(tileX - 34); 
	}

	
	
	public void update() {
		boolean intersects = false;
		speedX = bg.getSpeedX();
		tileX += speedX;
		r.setBounds(tileX, tileY, 40, 40);
		if (type != 0) {
			if (r.intersects(Mario.rectBot) ) {
				checkVerticalCollision(Mario.rectBot);
				intersects = true;
			}
			if(r.intersects(Mario.rectRight) ){
				checkSideCollision(Mario.rectRight);
				intersects = true;
			}
			if ( r.intersects(Mario.rectTop)) {
				checkVerticalCollision(Mario.rectTop);
				intersects = true;
			}
			if(r.intersects(Mario.rectLeft)){
				checkSideCollision(Mario.rectLeft);
				intersects = true;
			}
			if (intersects == false && mario.isReadyToWalk() == false && mario.isJumped() == true){
				if(mario.isMovingLeft())
					mario.setSpeedX(-5);
				if(mario.isMovingRight())
					mario.setSpeedX(5);
			}
		} 

		
		

//		if (r.intersects(Mario.rectBot) && type != 0) {
//			checkVerticalCollision(Mario.rectBot);
//			checkSideCollision(Mario.rectBot);
//		}
		
//		if (true && type != 0) {
//			checkVerticalCollision(Mario.rect);
//		}
	}

	public int getTileX() {
		return tileX;
	}

	public void setTileX(int tileX) {
		this.tileX = tileX;
	}

	public int getTileY() {
		return tileY;
	}

	public void setTileY(int tileY) {
		this.tileY = tileY;
	}

	public Image getTileImage() {
		return tileImage;
	}

	public void setTileImage(Image tileImage) {
		this.tileImage = tileImage;
	}

}
